package tests;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.*;
import static src.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import build.AbstractTestCase;
import page.IndexPage;
import page.ItemPage;
import page.ItemPopup;
import page.ModuleChequePaymentPage;
import page.OrderConfirmationPage;
import page.OrderPage;
import page.RegisterPage;
import page.form.ItemForm;
import page.form.RegisterPageForm;
import src.Util;

public class PurchaseFlowTests extends AbstractTestCase{
	public static IndexPage indexPage;
	public static ItemPage itemPage;
	public static ItemForm itemForm;
	public static ItemPopup itemPopup;
	public static OrderPage orderPage;
	public static RegisterPage registerPage;
	public static RegisterPageForm registerForm;
	public static RegisterPageForm deliveryForm;
	public static RegisterPageForm billingForm;
	public static ModuleChequePaymentPage chequePage;
	public static OrderConfirmationPage confirmationPage;
	public static String confirmationMessage;
	
	//All the click actions are performed here and the asserts make sure that the button did
	//what he is supposed to do
	@BeforeClass
	public static void testclick(){
		indexPage = new IndexPage();
		String value;
		assertNotNull("Item's value not being shown", value = indexPage.getItemValueByIndex(2));
		System.out.println("This is the value: "+value);
		assertNotNull("Not possible to click on the Item", itemPage = indexPage.clickItemByIndex(2));
		itemForm = new ItemForm();
		itemForm = itemPage.getItemInformation();
		assertNotNull("Add to Chart button not visible or doesn't exist", itemPopup = itemPage.clickAddToCart());
		assertNotNull("Proceed to Checkout button is not visible or doesn't exist", 
				orderPage = itemPopup.proceedToCheckoutButton());
		assertThat(itemForm.name, equalTo(orderPage.getItemName()));
		assertThat(itemForm.quantity, equalTo(orderPage.getItemQuantity()));
		assertThat(itemForm.price, equalTo(orderPage.getItemPrice()));
		assertThat(itemForm.finalPrice, equalTo(orderPage.getItemTotalPrice()));
		assertThat("Color not right", orderPage.getItemColorAndSize(), containsString(itemForm.color));
		assertThat("Size not right", orderPage.getItemColorAndSize(), containsString(itemForm.size));
		assertThat(orderPage.getTotalToPay(), equalTo
				(itemForm.finalPrice + (orderPage.getItemShippingPrice() + orderPage.getItemTaxPrice())));
		Double totalPlusTaxShipping = itemForm.finalPrice + (orderPage.getItemShippingPrice() + orderPage.getItemTaxPrice());
		assertNotNull("Proceed to checkout button is not visible or doesn't exists", 
				registerPage = orderPage.clickProceedToCheckoutButton());
		assertNotNull("Field to set email is not visible or does not exists", 
				registerPage.setEmail("Selenium"+Math.random()+"@gmail.com"));
		assertNotNull("Button to submit email and set personal information is not visible or does not exists", 
				registerPage.clickCreateAccountButton());
		assertNotNull("Title radio button not visible", registerPage.setTitleAsMister());
		assertNotNull("First name field not visible", registerPage.setFirstName("Selenium"));
		assertNotNull("Last name field not visible", registerPage.setLastName("Luz"));
		assertNotNull("Password field not visible", registerPage.setPassword("123456"));
		assertNotNull("Date of Birth Dropdown buttons not visible", registerPage.selectRandomBirthDate());
		assertNotNull("First name field on YOUR ADRESS section is not visible", registerPage.setFirstNameAgain("Selenium"));
		assertNotNull("Last name field on YOUR ADRESS section is not visible", registerPage.setLastNameAgain("Luz"));
		assertNotNull("Company field is not visible", registerPage.setCompany("Selenium S/A"));
		assertNotNull("Adress field is not visible", registerPage.setAdress("Selenium Junit Teste Street, 120"));
		assertNotNull("Adress field is not visible", registerPage.setAdress2("Java is better than PHP Avenue, 256"));
		assertNotNull("City field is not visible", registerPage.setCity("Selenium City"));
		assertNotNull("State dropdown button is not viisble", registerPage.setState());
		assertNotNull("Zip/Postal Code field is not visible", registerPage.setZipPostalCode());
		//assertNotNull("Country dropdown button is not visible", registerPage.setCountry());
		assertNotNull("Mobile phone field is not visible", registerPage.setMobilePhone());
		assertNotNull("Home phone field is not visible", registerPage.setHomePhone());
		assertNotNull("Adress reference field is not visible", registerPage.setAdressReference());
		registerForm = new RegisterPageForm();
		registerForm = registerPage.getRegisterInformation();
		assertNotNull("Could not finish the registration properly", registerPage.clickFinishCreateAccountButton());
		Util.wait(3000);
		registerPage = new RegisterPage();
		deliveryForm = new RegisterPageForm();
		deliveryForm = registerPage.getDeliveryInformation();
		billingForm = new RegisterPageForm();
		billingForm = registerPage.getBillingInformation();
		assertThat(registerForm.adress, equalTo(deliveryForm.adress));
		assertThat(registerForm.adress, equalTo(billingForm.adress));
		assertNotNull("Proceed to shipping phase button is not visible", orderPage = registerPage.proceedToShippingButton());
		assertNotNull("Accept Terms button is not visible", orderPage.clickAcceptTermsButton());
		assertNotNull("Proceed to Payment phase button is not visible", orderPage = orderPage.clickProceedToPaymentButton());
		assertThat(totalPlusTaxShipping, equalTo(orderPage.getTotalToPayOnPayment()));
		assertNotNull("Button to pay by check is not visible", chequePage = orderPage.clickPayByCheckButton());
		assertNotNull("Payment confirmation button not visible", confirmationPage = chequePage.clickConfirmOrderButton());
		confirmationMessage = confirmationPage.getFinishMessage();
		
	}
	
	@Test
	public void orderCompleteMessageTest(){
		assertThat(confirmationMessage, containsString("complete"));
	}
	
	
	
}
