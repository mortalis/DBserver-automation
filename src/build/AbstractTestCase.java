package build;

import org.openqa.selenium.WebDriver;

import src.Util;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
public class AbstractTestCase {
	public static WebDriver driver;
	public static Properties properties = new Properties(); 
	public static boolean isSuit;
	public static String path;
	
	
	
	public AbstractTestCase(String name) {
		super();
	}
	
	@BeforeClass
	public static void initialize(){
		
		if (driver == null) {
			try {
				properties.load(new FileInputStream("src/config.properties"));

				driver = Util.initialize(properties.getProperty("address"));
			} catch (IOException e) {
				System.out.println("Not possible to load config file, using localhost:8080 as url.");
				e.printStackTrace();
				driver = Util.initialize("http://localhost:8000/");
			}
			
		}
		
		
	}
	
	public AbstractTestCase() {
		super();

	}
	
	@AfterClass
	public static void closeDriver() {
		if (!isSuit) {
			Util.takeScreenshot("before close");
			driver.close();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// driver.quit();

		}
	}
	
	
	
}

