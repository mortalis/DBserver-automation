package page;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import src.Util;

public class OrderPage extends AbstractTestCase{
	protected WebElement orderPage;
	By order = By.id("order");
	By shippingPrice = By.id("total_shipping");
	By taxPrice = By.id("total_tax");
	By productName = By.cssSelector(".product-name a");
	By acceptTermsButton = By.id("cgv");
	By totalToPayOnPayment = By.id("total_price_container");
	
	public OrderPage(){
		super();
		orderPage = driver.findElement(order);
		assertNotNull("This is not the order page", orderPage);
	}
	
	public String getItemName(){
		return orderPage.findElement(By.xpath("//*[@class='cart_item last_item first_item address_0 odd']/td[2]/p/a")).getText();
	}
	
	public String getItemColorAndSize(){
		return orderPage.findElement(By.xpath("//*[@class='cart_item last_item first_item address_0 odd']/td[2]/small[2]/a")).getText();
	}
	
	public Double getItemPrice(){
		String value = "";
		String total = orderPage.findElement(By.xpath("//*[@class='cart_unit']/span")).getText();
		for (int i=1; i<total.length(); i++){
			value+=total.charAt(i);
		}
		
		return Double.parseDouble(value);
		
	}
	
	public int getItemQuantity(){
		return Integer.parseInt(orderPage.findElement(
				By.xpath("//*[@class='cart_item last_item first_item address_0 odd']/td[5]/input[2]"))
				.getAttribute("value"));
	}
	
	public Double getItemTotalPrice(){
		String value ="";
		String total = orderPage.findElement(By.xpath("//*[@class='cart_total']/span")).getText();
		for (int i=1; i<total.length(); i++){
			value+=total.charAt(i);
		}
		return Double.parseDouble(value);
	}
	
	public Double getItemShippingPrice(){
		String value = "";
		String total = orderPage.findElement(shippingPrice).getText();
		for (int i=1; i<total.length(); i++){
			value+=total.charAt(i);
		}
		
		return Double.parseDouble(value);
	}
	
	public Double getItemTaxPrice(){
		String value = "";
		String total = orderPage.findElement(taxPrice).getText();
		for (int i=1; i<total.length(); i++){
			value+=total.charAt(i);
		}
		
		return Double.parseDouble(value);
	}
	
	public Double getTotalToPay(){
		return getItemPrice() + getItemShippingPrice() + getItemTaxPrice();
	}
	
	public RegisterPage clickProceedToCheckoutButton(){
		try{
			orderPage.findElement(By.xpath("//*[@id='center_column']/p[2]/a[1]/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public OrderPage clickAcceptTermsButton(){
		try{
			orderPage.findElement(acceptTermsButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new OrderPage();
	}
	
	public OrderPage clickProceedToPaymentButton(){
		try{
			orderPage.findElement(By.xpath("//*[@id='form']/p/button")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new OrderPage();
	}
	
	public Double getTotalToPayOnPayment(){
		String value = "";
		String total = orderPage.findElement(totalToPayOnPayment).getText();
		for (int i=1; i<total.length(); i++){
			value+=total.charAt(i);
		}
		System.out.println("valooor"+value);
		return Double.parseDouble(value);
	}
	
	public ModuleChequePaymentPage clickPayByCheckButton(){
		try{
			orderPage.findElement(By.xpath("//*[@id='HOOK_PAYMENT']/div[2]/div/p")).click();
			Util.wait(2000);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ModuleChequePaymentPage();
	}
}
