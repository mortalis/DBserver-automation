package page;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import build.AbstractTestCase;
import page.form.ItemForm;
import src.Util;

public class ItemPage extends AbstractTestCase{

	protected WebElement itemPage;
	By colorSelected = By.cssSelector(".selected");
	By product = By.id(("product"));
	By addToCartButton = By.id("add_to_cart");
	public ItemPage(){
		super();
		Util.wait(3000);
		itemPage = driver.findElement(product);
		assertNotNull("N�o � o form do produto", itemPage);
	}
	
	public String getItemName(){
		return itemPage.findElement(By.xpath("//*[@id='center_column']/div/div/div[3]/h1")).getText();
	}
	
	public String getItemColor(){
		return itemPage.findElement(colorSelected).findElement(By.cssSelector("a")).getAttribute("title");
				//(By.xpath("//*[@id='color_to_pick_list']/li[1]/a")).getAttribute("name");
	}
	
	public String getItemSize(){
		Select select = new Select(itemPage.findElement(By.xpath("//*[@id='group_1']")));
		WebElement option = select.getFirstSelectedOption();
		String size = option.getText();
		return size;
	}
	
	public int getItemQuantity(){
		return Integer.parseInt(itemPage.findElement(By.xpath("//*[@id='quantity_wanted']")).getAttribute("value"));
	}
	
	public Double getItemPrice(){
		String value="";
		String itemValue = itemPage.findElement(By.xpath("//*[@id='our_price_display']")).getText();
		for (int i=1; i<itemValue.length(); i++){
			value+=itemValue.charAt(i);
		}
		return Double.parseDouble(value);
		
	}
	
	public Double getItemTotalPrice(){
		return getItemPrice()*getItemQuantity();
	}
	
	public ItemForm getItemInformation(){
		ItemForm itemForm;
		itemForm = new ItemForm();
		itemForm.addItemFormInformation(getItemName(), getItemColor(), getItemSize(), getItemQuantity(), getItemPrice(),
				getItemTotalPrice());
		return itemForm;
	}
	
	public ItemPopup clickAddToCart(){
		try{
			itemPage.findElement(addToCartButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new ItemPopup();
	}
}
