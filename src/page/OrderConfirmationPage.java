package page;

import static org.junit.Assert.assertNotNull;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;

public class OrderConfirmationPage extends AbstractTestCase{
	protected WebElement confirmationPage;
	By orderConfirmation = By.id("order-confirmation");
	
	public OrderConfirmationPage(){
		super();
		confirmationPage = driver.findElement(orderConfirmation);
		assertNotNull("This is not the payment confirmation page", orderConfirmation);
	}
	
	public String getFinishMessage(){
		String teste = confirmationPage.findElement(By.xpath("//*[@id='center_column']/p[1]")).getText();
		System.out.println("mensagem: "+teste);
		return teste;
	}
}
