package page;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import src.Util;

public class ModuleChequePaymentPage extends AbstractTestCase{
	protected WebElement modulePayment;
	By moduleCheque = By.id("module-cheque-payment");
	public ModuleChequePaymentPage(){
		super();
		modulePayment = driver.findElement(moduleCheque);
		assertNotNull("This is not the cheque payment page", modulePayment);
	}
	
	public OrderConfirmationPage clickConfirmOrderButton(){
		try{
			modulePayment.findElement(By.xpath("//*[@id='cart_navigation']/button")).click();
			Util.wait(2000);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new OrderConfirmationPage();
	}
}
