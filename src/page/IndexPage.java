package page;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import build.AbstractTestCase;

public class IndexPage extends AbstractTestCase{
	
	protected WebElement indexPage;
	
	
	public IndexPage(){
		super();
		indexPage = driver.findElement(By.id("index"));
		assertNotNull("N�o � a p�gina inicial", indexPage);
	}
	
	public ItemPage clickItemByIndex(int index){
		try{
			Actions action = new Actions(driver);
			  action.moveToElement(indexPage.findElement(By.xpath("//*[@id='homefeatured']/li["+index+"]/div/div[1]/div")))
			  .perform();
			  indexPage.findElement(By.xpath("//*[@id='homefeatured']/li["+index+"]/div/div[2]/div[2]/a[2]")).click();
			//*[@id="homefeatured"]/li[2]/div/div[2]/div[2]/a[2]
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}
		return new ItemPage();
	}
	
	public String getItemValueByIndex(int index){
		String value;
		try{
			Actions action = new Actions(driver);
			  action.moveToElement(indexPage.findElement(By.xpath("//*[@id='homefeatured']/li["+index+"]/div/div[1]/div")))
			  .perform();
			 value = indexPage.findElement(By.xpath("//*[@id='homefeatured']/li["+index+"]/div/div[1]/div/div[2]")).getText();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return value;
	}
	
}
