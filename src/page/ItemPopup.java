package page;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import build.AbstractTestCase;
import src.Util;

public class ItemPopup extends AbstractTestCase {
	protected WebElement itemPopup;
	By popup = By.cssSelector(".clearfix");
	public ItemPopup(){
		super();
		itemPopup = driver.findElement(popup);
		assertNotNull("The item popup didn't open", itemPopup);
	}
	
	public OrderPage proceedToCheckoutButton(){
		try{
			Util.wait(3000);
			itemPopup.findElement(By.xpath("//*[@id='layer_cart']/div[1]/div[2]/div[4]/a")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new OrderPage();
	}
}
