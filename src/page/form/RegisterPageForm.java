package page.form;

import src.ComparableForm;

public class RegisterPageForm extends ComparableForm{
	public String fullName, company, adress, cityStatePostCode, countryName, phone, mobilePhone;
	
	public void AddRegisterPageForm(String fullName, String company, String adress, String cityStatePostCode,
			String countryName, String phone, String mobilePhone){
		this.fullName = fullName;
		this.company = company;
		this.adress = adress;
		this.cityStatePostCode = cityStatePostCode;
		this.countryName = countryName;
		this.phone = phone;
		this.mobilePhone = mobilePhone;
	}
}
