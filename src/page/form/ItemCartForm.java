package page.form;

public class ItemCartForm {

	public String name, colorAndSize, price;
	public int quantity;
	public Double total, tax, totalPlusTax;
	
	public void addItemInformationOnCart(String name, String colorAndSize, 
			 String price, int quantity, Double total, Double tax, Double totalPlusTax){
		this.name = name;
		this.colorAndSize = colorAndSize;
		this.price = price;
		this.quantity = quantity;
		this.total = total;
		this.tax = tax;
		this.totalPlusTax = totalPlusTax;
	}
}
