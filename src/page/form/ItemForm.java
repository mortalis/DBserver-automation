package page.form;

public class ItemForm {
	public String name, color, size;
	public int quantity;
	public Double finalPrice, price;
	
	public void addItemFormInformation(String name, String color, String size, int quantity, Double price, Double finalPrice){
		this.name = name;
		this.color = color;
		this.size = size;
		this.quantity = quantity;
		this.price = price;
		this.finalPrice = finalPrice;
	}
	
}
