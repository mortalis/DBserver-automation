package page;

import static org.junit.Assert.assertNotNull;

import java.util.NoSuchElementException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import build.AbstractTestCase;
import page.form.RegisterPageForm;
import src.Util;

public class RegisterPage extends AbstractTestCase{
	protected WebElement registerPage;
	By pageOfRegister = By.id("page");
	By emailField = By.id("email_create");
	By createAccountButton = By.id("SubmitCreate");
	By misterRadioButton = By.id("uniform-id_gender1");
	By firstNameField = By.id("customer_firstname");
	By lastNameField = By.id("customer_lastname");
	By passwordField = By.id("passwd");
	By days = By.id("days");
	By months = By.id("months");
	By years = By.id("years");
	By firstNameAgain = By.id("firstname");
	By lastNameAgain = By.id("lastname");
	By companyField = By.id("company");
	By adressField = By.id("address1");
	By adress2Field = By.id("address2");
	By cityField = By.id("city");
	By stateDropdown = By.id("id_state");
	By postalCode = By.id("postcode");
	By countryDropdown = By.id("id_country");
	By phoneMobile = By.id("phone_mobile");
	By homePhone = By.id("phone");
	By adressReference = By.id("alias");
	By finishAccountCreationButton = By.id("submitAccount");
	public RegisterPage(){
		super();
		registerPage = driver.findElement(pageOfRegister);
		assertNotNull("This is not the Register Page", registerPage);
	}
	
	public RegisterPage setEmail(String email){
		try{
			registerPage.findElement(emailField).clear();
			registerPage.findElement(emailField).sendKeys(email);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage clickCreateAccountButton(){
		try{
			registerPage.findElement(createAccountButton).click();
			Util.wait(3000);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setTitleAsMister(){
		try{
			registerPage.findElement(misterRadioButton).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setFirstName(String name){
		try{
			registerPage.findElement(firstNameField).clear();
			registerPage.findElement(firstNameField).sendKeys(name);;
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setLastName(String lastName){
		try{
			registerPage.findElement(lastNameField).clear();
			registerPage.findElement(lastNameField).sendKeys(lastName);;
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setPassword(String password){
		try{
			registerPage.findElement(passwordField).clear();
			registerPage.findElement(passwordField).sendKeys(password);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage selectRandomBirthDate() {
		WebElement element;
		element = registerPage.findElement(days);
		Select selectDays = new Select(element);
		if (selectDays.getOptions().size() == 0) {
			return null;
		} else {
			Random rand = new Random();
			selectDays.selectByIndex(rand.nextInt(selectDays.getOptions().size()));
				
		}
		
		element = registerPage.findElement(months);
		Select selectMonths = new Select(element);
		if (selectMonths.getOptions().size() == 0) {
			return null;
		} else {
			Random rand = new Random();
			selectMonths.selectByIndex(rand.nextInt(selectMonths.getOptions().size()));
				
		}
		
		element = registerPage.findElement(years);
		Select selectYear = new Select(element);
		if (selectYear.getOptions().size() == 0) {
			return null;
		} else {
			Random rand = new Random();
			selectYear.selectByIndex(rand.nextInt(selectYear.getOptions().size()));
				
		}
		Util.wait(1000);
		return new RegisterPage();
	}
	
	public RegisterPage setFirstNameAgain(String name){
		try{
			registerPage.findElement(firstNameAgain).clear();
			registerPage.findElement(firstNameAgain).sendKeys(name);;
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setLastNameAgain(String lastName){
		try{
			registerPage.findElement(lastNameAgain).clear();
			registerPage.findElement(lastNameAgain).sendKeys(lastName);;
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setCompany(String company){
		try{
			registerPage.findElement(companyField).clear();
			registerPage.findElement(companyField).sendKeys(company);;
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setAdress(String adress){
		try{
			registerPage.findElement(adressField).clear();
			registerPage.findElement(adressField).sendKeys(adress);;
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setAdress2(String adress){
		try{
			registerPage.findElement(adress2Field).clear();
			registerPage.findElement(adress2Field).sendKeys(adress);;
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setCity(String city){
		try{
			registerPage.findElement(cityField).clear();
			registerPage.findElement(cityField).sendKeys(city);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setState(){
		WebElement element = registerPage.findElement(stateDropdown);
		Select select = new Select(element);
		if (select.getOptions().size() == 0) {
			return null;
		} else {
			Random rand = new Random();
			select.selectByIndex(rand.nextInt(select.getOptions().size()));
				
		}
		return new RegisterPage();
	}
	
	public RegisterPage setZipPostalCode(){
		try{
			registerPage.findElement(postalCode).clear();
			registerPage.findElement(postalCode).sendKeys("00000");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setCountry(){
		WebElement element = registerPage.findElement(countryDropdown);
		Select select = new Select(element);
		if (select.getOptions().size() == 0) {
			return null;
		} else {
			Random rand = new Random();
			select.selectByIndex(rand.nextInt(select.getOptions().size()));
				
		}
		return new RegisterPage();
	}
	
	public RegisterPage setMobilePhone(){
		try{
			registerPage.findElement(phoneMobile).clear();
			registerPage.findElement(phoneMobile).sendKeys("123456789");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setHomePhone(){
		try{
			registerPage.findElement(homePhone).clear();
			registerPage.findElement(homePhone).sendKeys("123456789");
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage setAdressReference(){
		try{
			registerPage.findElement(adressReference).clear();
			registerPage.findElement(adressReference).sendKeys("Selenium"+Math.random());
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public RegisterPage clickFinishCreateAccountButton(){
		try{
			registerPage.findElement(finishAccountCreationButton).click();
			Util.wait(3000);
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new RegisterPage();
	}
	
	public String getFullName(){
		String fullName = "";
		fullName = registerPage.findElement(firstNameField).getAttribute("value");
		fullName+= " "+registerPage.findElement(lastNameField).getAttribute("value");
		return fullName;
	}
	
	public String getCompany(){
		String company = registerPage.findElement(companyField).getAttribute("value");
		return company;
	}
	
	public String getAdresses(){
		String adress = registerPage.findElement(adressField).getAttribute("value");
		adress+=" "+registerPage.findElement(adress2Field).getAttribute("value");
		return adress;
	}
	
	public String getCityStatePostalCode(){
		String cityStatePostal = registerPage.findElement(cityField).getAttribute("value");
		Select select = new Select(registerPage.findElement(stateDropdown));
		WebElement option = select.getFirstSelectedOption();
		cityStatePostal+=", "+option.getText()+" "
				+registerPage.findElement(postalCode).getAttribute("value");
		return cityStatePostal;
	}
	
	public String getCountryName(){
		Select select = new Select(registerPage.findElement(countryDropdown));
		WebElement option = select.getFirstSelectedOption();
		return option.getText();
	}
	
	public String getMobilePhoneNumber(){
		return registerPage.findElement(phoneMobile).getAttribute("value");
	}
	
	public String getPhoneNumber(){
		return registerPage.findElement(homePhone).getAttribute("value");
	}
	
	public RegisterPageForm getRegisterInformation(){
		RegisterPageForm registerForm = new RegisterPageForm();
		registerForm.AddRegisterPageForm(getFullName(), getCompany(), getAdresses(), 
				getCityStatePostalCode(), getCountryName(), getPhoneNumber(), getMobilePhoneNumber());
		return registerForm;
	}
	
	public String getDeliveryFullName(){
		String teste="";
		try{
			teste = registerPage.findElement(By.xpath("//*[@id='address_delivery']/li[2]")).getText();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}
		
		return teste;
	}
	
	public String getDeliveryCompany(){
		return registerPage.findElement(By.xpath("//*[@id='address_delivery']/li[3]")).getText();
	}
	
	public String getDeliveryAdresses(){
		return registerPage.findElement(By.xpath("//*[@id='address_delivery']/li[4]")).getText();
	}
	
	public String getDeliveryCityStatePostalCode(){
		return registerPage.findElement(By.xpath("//*[@id='address_delivery']/li[5]")).getText();
	}
	
	public String getDeliveryCountry(){
		return registerPage.findElement(By.xpath("//*[@id='address_delivery']/li[6]")).getText();
	}
	
	public String getDeliveryPhone(){
		return registerPage.findElement(By.xpath("//*[@id='address_delivery']/li[7]")).getText();
	}
	
	public String getDeliveryMobilePhone(){

		return registerPage.findElement(By.xpath("//*[@id='address_delivery']/li[8]")).getText();
	}
	
	public String getBillingFullName(){
		String teste="";
		try{
			teste = registerPage.findElement(By.xpath("//*[@id='address_invoice']/li[2]")).getText();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}
		
		return teste;
	}
	
	public String getBillingCompany(){
		return registerPage.findElement(By.xpath("//*[@id='address_invoice']/li[3]")).getText();
	}
	
	public String getBillingAdresses(){
		return registerPage.findElement(By.xpath("//*[@id='address_invoice']/li[4]")).getText();
	}
	
	public String getBillingCityStatePostalCode(){
		return registerPage.findElement(By.xpath("//*[@id='address_invoice']/li[5]")).getText();
	}
	
	public String getBillingCountry(){
		return registerPage.findElement(By.xpath("//*[@id='address_invoice']/li[6]")).getText();
	}
	
	public String getBillingPhone(){
		return registerPage.findElement(By.xpath("//*[@id='address_invoice']/li[7]")).getText();
	}
	
	public String getBillingMobilePhone(){
		return registerPage.findElement(By.xpath("//*[@id='address_invoice']/li[8]")).getText();
	}
	
	public RegisterPageForm getDeliveryInformation(){
		RegisterPageForm registerForm = new RegisterPageForm();
		registerForm.AddRegisterPageForm(getDeliveryFullName(), getDeliveryCompany(), getDeliveryAdresses(), getDeliveryCityStatePostalCode(),
				getDeliveryCountry(), getDeliveryPhone(), getDeliveryMobilePhone());
		return registerForm;
	}
	
	public RegisterPageForm getBillingInformation(){
		RegisterPageForm registerForm = new RegisterPageForm();
		registerForm.AddRegisterPageForm(getBillingFullName(), getBillingCompany(), getBillingAdresses(), getBillingCityStatePostalCode(),
				getBillingCountry(), getBillingPhone(), getBillingMobilePhone());
		return registerForm;
	}
	
	public OrderPage proceedToShippingButton(){
		try{
			registerPage.findElement(By.xpath("//*[@id='center_column']/form/p/button/span")).click();
		}catch(NoSuchElementException | ElementNotVisibleException e){
			return null;
		}return new OrderPage();
	}
	
}
