<p><strong>Para rodar as classes de teste presentes nesse projeto voc&ecirc; precisar&aacute;:</strong></p>

<ol>
	<li>Usar Sistema Operacional Linux ou Windows;</li>
	<li>Possuir o navegador Google Chrome instalado na sua m&aacute;quina;</li>
	<li>Possuir a IDE <strong>Eclipse </strong>instalada;</li>
	<li>Ferramenta de versionamento instalada;</li>
</ol>

<p><strong>Passo &agrave; passo:</strong></p>

<ol>
	<li>Na ferramenta de versionamento, v&aacute; at&eacute; o diret&oacute;rio em que deseja clonar o projeto;</li>
	<li>git clone url_do_reposit&oacute;rio;</li>
	<li>Abra o Eclipse;</li>
	<li>Clique em <strong>FILE-&gt;IMPORT</strong>;</li>
	<li>Selecione a op&ccedil;&atilde;o Projects from Git;</li>
	<li>Em seguida selecione Existing local repository;</li>
	<li>V&aacute; at&eacute; o reposit&oacute;rio onde o projeto foi clonado e o selecione, logo ap&oacute;s clicando em finish;</li>
	<li>Espere o Eclipse carregar os arquivos;</li>
	<li>Terminada essa parte, expanda as pastas do projeto, dentro do Eclipse;</li>
	<li>V&aacute; em <strong>automation-&gt;src-&gt;tests-&gt;PurchaseFlowTests.java</strong> e d&ecirc; dois cliques nesse &uacute;ltimo arquivo, esse &eacute; o arquivo dos testes;</li>
	<li>Clique com o bot&atilde;o direito em cima do nome do arquivo e v&aacute; em <strong>Run As -&gt; JUnit Test</strong>;</li>
</ol>

<p>Pronto, logo ap&oacute;s fazer isso, o Google Chrome deve abrir automaticamente, navegar pela p&aacute;gina setada no arquivo config.properties e come&ccedil;ar a fazer os testes.<br />
Quando terminar, a aba do navegador se fechar&aacute; e voc&ecirc; ter&aacute; a resposta dos testes dentro do Eclipse, na aba de JUnit.</p>